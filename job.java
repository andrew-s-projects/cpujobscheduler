/*
/      filename:  Job.java
/
/   description:  Contains necessary data fields for each job
 */
package project356;

/**
 * Class containing all the jobs and their various data fields
 */
public class Job implements Comparable<Job> {

    final int max_Mem = 512;
    String event;
    private String finalList;
    private int underMax;
    String status;
    private int arrivalTime;
    int jobNo;
    private int memory;
    int runTime;
    int burstTime;
    private int startTime;
    private int compTime;
    private int lastTime;
    private int originalRunTime;
    private int wait;
    private int IOStart;
    private int theBurst;
    private int IOCompTime;
    private int IOQuantum;
    int qLevel;
    String prompt = "This job exceeds the system's main memory capacity.";

    Job() {
    }

    ;
    
    //A job of event type A
    Job(String event, int time, int jobNo, int memory, int runTime) {
        this.event = event;
        this.arrivalTime = time;
        this.jobNo = jobNo;
        this.memory = memory;
        this.runTime = runTime;
        lastTime = time;
        originalRunTime = runTime;
        startTime = 0;
        qLevel = 1;
        wait = 0;
        IOQuantum = 0;
    }

    /*A job of event type D (displays status of simulator)
    "Job" also refers to "event" used for event queue
     */
    Job(String event, int time) {
        this.event = event;
        arrivalTime = time;
        IOQuantum = 0;
    }

    //A job of event type I (perform I/O)
    Job(String event, int time, int burst) {
        this.event = event;
        arrivalTime = time;
        burstTime = burst;
        IOQuantum = 0;
    }

    Job(Job job) {
        this.event = job.event;
        this.arrivalTime = job.arrivalTime;
    }

    /*
    To Line 140: Methods used to set data fields of job
     */
    void setStartTime(int time) {
        startTime = time;
    }

    public void setRun(int time) {
        runTime = time;
    }

    public void setWait(int time) {
        wait = time;
    }

    public void setStatus(String str) {
        status = str;
    }

    public void setCompTime(int time) {
        compTime = time;
    }

    public void setMemory(int mem) {
        memory = mem;
    }

    public void setLastTime(int time) {
        lastTime = time;
    }

    public void setIOQuantum(int time) {
        IOQuantum = time;
    }

    public void setQLevel(int number) {
        qLevel = number;
    }

    public void setBurstTime(int time) {
        burstTime = time;
    }

    public void setIOStartTime(int time) {
        IOStart = time;
    }

    public void setBurst(int time) {
        theBurst = time;
    }

    public void setIOCompTime(int time) {
        IOCompTime = time;
    }

    /*
    To Line 208: returns the data fields of job
     */
    public String getStatus() {
        return status;
    }

    public int getMemory() {
        return memory;
    }

    public String getEvent() {
        return event;
    }

    public int getLastTime() {
        return lastTime;
    }

    public int getWait() {
        return wait;
    }

    public int getStartTime() {
        return startTime;
    }

    public int getQLevel() {
        return qLevel;
    }

    public int getBurstTime() {
        return burstTime;
    }

    public int getIOQuantum() {
        return IOQuantum;
    }

    public int getRun() {
        return runTime;
    }

    public int getCompTime() {
        return compTime;
    }

    public int getIOStartTime() {
        return IOStart;
    }

    public int getIOCompleteTime() {
        return IOCompTime;
    }

    public int getArrival() {
        return arrivalTime;
    }

    public int getJobNo() {
        return jobNo;
    }

    public int getOriginalRun() {
        return originalRunTime;
    }

    /*
    To line 237: returns string with required data for printing of different queues
     */
    public String finalList() {
        return jobNo + "        " + arrivalTime + "       " + memory + "        " + originalRunTime + "         "
                + startTime + "         " + compTime;
    }

    public String IOQ() {
        return jobNo + "        " + arrivalTime + "        " + memory + "        " + originalRunTime + "        " + IOStart + "        "
                + burstTime + "        " + IOCompTime;
    }

    public String toString() {
        if (event.equals("This job exceeds the system's main memory capacity.")) {
            return "Event: " + "A " + "  " + "Time: " + arrivalTime + "\n" + prompt;
        }
        return "Event: " + event + "   " + "Time: " + arrivalTime;

    }

    public String jobQ() {
        return jobNo + "        " + arrivalTime + "       " + memory + "        " + runTime;

    }

    public String CPU() {
        return jobNo + "        " + startTime + "        " + runTime;
    }

    /*
    Used to organize eventQ: compares arrival time of events
     */
    public int compareTo(Job o) {
        return this.arrivalTime - o.arrivalTime;
    }
}
