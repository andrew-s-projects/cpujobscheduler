package project356;

public class Queue {

    list listofElements;
    int index;
    int count;
    Job value;
    Job temp;

    //implement queue by list, initialize a new list
    Queue() {
        listofElements = new list();
    }

    //use list.insert to add element to list(queueu)
    void enq(int number) {
        listofElements.insert(number, index);
        index = index + 1;
    }

    /*
    Adds job to queue
     */
    void addJob(Job job) {
        listofElements.insert(job, index);
        index = index + 1;
        count++;
    }

    /*
    Peek the first element on the queue
     */
    Job peak() {
        temp = listofElements.peak(0);

        return temp;
    }

    /*
    return size of queue
     */
    int getSize() {
        return index;
    }

    Job dq() {
        value = listofElements.delete(0);
        index--;
        return value;

    }

    /*
    Print methods print contents of respective queue
     */
    void printJobQ() {
        listofElements.printJobQStatus();
    }

    public void printCPU() {
        listofElements.printCPU();
    }

    public void printReadyQ() {
        listofElements.printReadyQ();
    }

    public void printReadyQ2() {
        listofElements.printReadyQ2();
    }

    public void sort() {
        listofElements = new list(listofElements.sortList());
    }

    public void updateWait() {
        listofElements.updateWaitTime();
    }

    public boolean hasIO() {
        return listofElements.hasIO();
    }

    void print() {
        listofElements.print();
    }
}
