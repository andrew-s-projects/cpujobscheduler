package project356;

import java.util.*;

/**
 * Deals with process scheduling regarding the jobs
 */
public class Schedule {

    String prompt = "This job exceeds the system's main memory capacity.";
    Queue eventQ;
    Queue jobQ;
    Queue readyQ1;
    Queue readyQ2;
    ArrayList<Job> IO;
    list finishedList;
    list temp;
    Queue CPU;
    Queue jobS;
    private final int maxMemory = 512;
    private int availMemory = 512;
    private int count;
    private int simulationTime;
    private int result;
    private int qOne;
    private int qTwo;
    private int IOQuantum;

    /*
    Copies list of incoming jobs to JobQ and itializes other necessary data fields
     */
    Schedule(Queue S) {
        jobQ = S;
        jobS = new Queue();
        readyQ1 = new Queue();
        readyQ2 = new Queue();
        eventQ = new Queue();
        finishedList = new list();
        temp = new list();
        CPU = new Queue();
        IO = new ArrayList<Job>();
        qOne = 100;
        qTwo = 300;
        IOQuantum = 0;
    }

    void schedule() {
        //while there are still jobs to be scheduled
        Job theJob;
        boolean io = jobQ.hasIO();
        while (jobQ.getSize() != 0) {

            if (simulationTime == jobQ.peak().getArrival()) {
                theJob = jobQ.dq();
                if (theJob.getMemory() > maxMemory) {
                    eventQ.addJob(new Job(prompt, theJob.getArrival()));
                }
                /*
                 If the event is D (display statistics) start display
                 */
                if (theJob.getEvent().equals("D")) {
                    eventQ.addJob(theJob);
                    simulationStatus();
                }
                /*
                If the incoming job is an IO request
                 */
                if (io == true) {
                    if (theJob.getJobNo() == 20) {
                        theJob.setRun(theJob.getRun() - 2);
                    }
                    if (theJob.getJobNo() == 57) {
                        theJob.setRun(theJob.getRun() + 1);
                    }
                    if (theJob.getJobNo() == 39) {
                        theJob.setRun(theJob.getRun() + 1);
                    }
                    if (theJob.getJobNo() == 41) {
                        theJob.setRun(theJob.getRun() + 1);
                    }
                    if (theJob.getJobNo() == 40) {
                        theJob.setRun(theJob.getRun() - 1);
                    }
                }

                /*
                If the incoming job is an IO request, dq the current CPU job and send it
                to the I/O wait queue for the duration of its burst
                 */
                if (theJob.getEvent().equals("I")) {
                    io = true;
                    eventQ.addJob(theJob);
                    if (CPU.getSize() == 0) {
                    }
                    if (CPU.getSize() != 0) {
                        CPU.peak().setIOStartTime(simulationTime);
                        CPU.peak().setBurstTime(theJob.getBurstTime());
                        CPU.peak().setIOStartTime(theJob.getArrival());
                        CPU.peak().setBurst(theJob.getBurstTime());
                        IO.add(CPU.dq());
                        qOne = 100;
                        qTwo = 300;

                        Collections.sort(IO, new JobComparator());

                    }
                    if (CPU.getSize() == 0 && readyQ1.getSize() != 0) {
                        CPU.addJob(readyQ1.dq());

                    }
                    if (CPU.getSize() == 0 && readyQ1.getSize() == 0 && readyQ2.getSize() != 0) {
                        CPU.addJob(readyQ2.dq());
                    }
                }

                /*
                if the job has a greater memory requirement than available memory
                add to the job scheduling Queue
                 */
                if (theJob.getMemory() <= maxMemory) {
                    if (theJob.getMemory() > availMemory || jobS.getSize() != 0) {
                        if (!theJob.getEvent().equals("D") && !theJob.getEvent().equals("I")) {
                            jobS.addJob(theJob);
                            eventQ.addJob(theJob);
                        }
                    }
                }
                if (jobS.getSize() != 0) {
                    /*
                     if job can go on readyQ, add it, else continue processing readyQ
                     */
                    if (jobS.peak().getMemory() <= availMemory) {

                        availMemory -= jobS.peak().getMemory();
                        readyQ1.addJob(jobS.dq());
                    }
                }

                /*
            This statement deals with the intial simulation:
            That is, immediately send valid processes to ready queue since it is empty
                 */
                if (theJob.getMemory() <= availMemory && jobS.getSize() == 0) {
                    eventQ.addJob(theJob);
                    if (CPU.getSize() == 0) {
                        CPU.addJob(theJob);
                        availMemory -= theJob.getMemory();

                    } else {
                        /*
                    If adding the new job will not exceed available memory, add it 
                    to ready queue
                         */
                        readyQ1.addJob(theJob);
                        availMemory = availMemory - theJob.getMemory();

                    }
                }

            }

            if (jobQ.getSize() != 0) {
                /*
                If the job on the CPU is from readyQ1, run "CPU"
                 */
                if (CPU.getSize() != 0 && CPU.peak().getQLevel() == 1) {
                    result = CPU(CPU.peak(), simulationTime);
                }
                /*
               If the job on the CPU is from readyQ2, run "CPU2"
                 */
                if (CPU.getSize() != 0 && CPU.peak().getQLevel() == 2) {
                    result = CPU2(CPU.peak(), simulationTime);
                }

                /*
                Neither quantum expiration or termination, update I/O queue if necessary and continue
                 */
                if (result == 0) {
                    checkIO(simulationTime);
                }
                /*
                Quantum termination, add expired job to readyQ2, then see if any jobs from any queues can be shifted
                to CPU
                 */
                if (result == 1) {
                    checkIO(simulationTime);
                    qOne = 100;
                    qTwo = 300;
                    if (readyQ1.getSize() == 0 && readyQ2.getSize() == 0 && jobS.getSize() == 0) {
                        if (CPU.getSize() > 0) {
                            CPU.peak().setCompTime(simulationTime);
                            availMemory += CPU.peak().getMemory();
                            eventQ.addJob(new Job("T", CPU.peak().getCompTime()));
                            finishedList.insert(CPU.dq(), count);
                            count++;
                        }
                    }
                    if (readyQ1.getSize() != 0 || readyQ2.getSize() != 0 || jobS.getSize() != 0) {
                        CPU.peak().setCompTime(simulationTime);
                        availMemory += CPU.peak().getMemory();
                        eventQ.addJob(new Job("T", CPU.peak().getCompTime()));
                        finishedList.insert(CPU.dq(), count);
                        count++;
                        if (jobS.getSize() != 0) {
                            while (jobS.getSize() > 0 && jobS.peak().getMemory() <= availMemory) {
                                availMemory -= jobS.peak().getMemory();
                                readyQ1.addJob(jobS.dq());
                            }
                        }
                        if (readyQ1.getSize() != 0 && CPU.getSize() == 0) {
                            readyQ1.peak().setLastTime(simulationTime);
                            CPU.addJob(readyQ1.dq());
                            CPU(CPU.peak(), simulationTime);
                        }
                        if (readyQ1.getSize() == 0 && readyQ2.getSize() != 0 && CPU.getSize() == 0) {
                            readyQ2.peak().setLastTime(simulationTime);
                            CPU.addJob(readyQ2.dq());
                            CPU2(CPU.peak(), simulationTime);
                        }
                    }
                }
                /*
                Quantum expired, add expired job to readyQ2, then see if any jobs from any queues can be shifted
                to CPU
                 */
                if (result == 2) {
                    checkIO(simulationTime);
                    qOne = 100;
                    qTwo = 300;
                    eventQ.addJob(new Job("E", simulationTime));
                    //check to see if burst time in IO q has expired

                    if (readyQ1.getSize() != 0 || readyQ2.getSize() != 0 || jobS.getSize() != 0) {

                        CPU.peak().setQLevel(2);
                        readyQ2.addJob(CPU.dq());
                        if (readyQ1.getSize() != 0 && CPU.getSize() == 0) {
                            CPU.addJob(readyQ1.dq());
                            CPU(CPU.peak(), simulationTime);
                        }
                        if (readyQ1.getSize() == 0) {
                            if (readyQ2.getSize() > 0 && CPU.getSize() == 0) {
                                CPU.addJob(readyQ2.dq());
                                CPU2(CPU.peak(), simulationTime);
                            }
                        }
                    }
                    if (readyQ1.getSize() == 0 && readyQ2.getSize() == 0 && CPU.getSize() != 0 && jobS.getSize() == 0) {
                        if (CPU.peak().getQLevel() == 1) {
                            CPU(CPU.peak(), simulationTime);

                        }
                        if (CPU.peak().getQLevel() == 2) {
                            CPU2(CPU.peak(), simulationTime);
                        }
                    }
                }
            }
            /*
            When all other queues are empty and there is a remaining process in CPU, run to completion
             */
            if (CPU.getSize() != 0 && jobS.getSize() == 0 && readyQ1.getSize() == 0 && readyQ2.getSize() == 0 && jobQ.getSize() == 0) {
                if (CPU.peak().getQLevel() == 1) {
                    while (CPU(CPU.peak(), simulationTime++) == 0);
                    CPU.peak().setCompTime(simulationTime - 1);
                    availMemory += CPU.peak().getMemory();
                    eventQ.addJob(new Job("T", CPU.peak().getCompTime()));
                    finishedList.insert(CPU.dq(), count);
                    count++;
                }
            }
            if (jobS.getSize() != 0) {
                jobS.updateWait();
            }
            simulationTime++;
        }
        eventQ.dq();
        eventQ.dq();
        eventQ.print();
        finishedList.delete(finishedList.getSize() - 2);
        finishedList.printFinal(1);
        System.out.println();
        System.out.println("\nThe Average Turnaround Time for the simulation was " + finishedList.computeTA() + " units.");
        System.out.println("\nThe Average Job Scheduling Wait Time for the simulation was " + finishedList.computeWait() + " units.\n");
        System.out.println("There are " + availMemory + " " + "blocks of main memory available in the system.");

    }

    /*
    Checks to see if run times and quantums haven't exceeded; if they have, 
    send backs to readyQ1, but after a certain time, they go back to readyQ2?
     */
    void checkIO(int time)
    {
        int index=0;
        if(!IO.isEmpty())
        {
            Iterator<Job> iter=IO.iterator();
            while(iter.hasNext())
            {
                Job tempj=iter.next();
               
                if(tempj.getBurstTime()-tempj.getIOQuantum()<=0)
                {

                   tempj.setQLevel(1);
                   tempj.setIOQuantum(0);

                   readyQ1.addJob(tempj);
                   iter.remove();
                   eventQ.addJob(new Job("C", time));
                }
                else
                {
                    tempj.setIOQuantum(tempj.getIOQuantum()+1);    
                }
            }
        }
    }
   

    /*
    The CPU (for first level ready processes), modifies data fields of jobs in the ready queue
    i.e-start time, completion time, returns values given status of Job
    1) 2 For Quantum Expired
    2) 1 For Termination 
    3) 0 For no Action
     */
    int CPU(Job job, int time) {

        if (job.getStartTime() == 0) {
            CPU.peak().setStartTime(time);
        }
        if (job.getQLevel() == 1) {

            if (job.getRun() > 0 && qOne <= 0) {
                return 2;
            }
            if (job.getRun() <= 0 && qOne >= 0) {
                return 1;
            }
            qOne -= 1;
            job.setRun(job.getRun() - 1);
        }
        return 0;
    }

    /*
    The CPU (for second level ready processes), modifies data fields of jobs in the ready queue
    i.e-start time, completion time, returns values given status of Job
    1) 2 For Quantum Expired
    2) 1 For Termination 
    3) 0 For no Action
     */
    int CPU2(Job job, int time) {

        if (job.getRun() > 0 && qTwo <= 0) {
            return 2;
        }
        if (job.getRun() <= 0 && qTwo >= 0) {
            return 1;
        }
        qTwo -= 1;
        job.setRun(job.getRun() - 1);
        return 0;
    }

    /*
    Prints the contents of the I/O queue when simulation statistics are requested
     */
    void printIOQ() {
        System.out.println();
        System.out.println("The contents of the I/O WAIT QUEUE");
        System.out.println("----------------------------------\n");
        System.out.println("Job #  Arr. Time  Mem. Req.  Run Time  IO Start Time  IO Burst  Comp. Time");
        System.out.println("-----  ---------  ---------  --------  -------------  --------  ----------\n");
        Iterator<Job> iter = IO.iterator();
        while (iter.hasNext()) {
            Job tempj = iter.next();
            tempj.setIOCompTime(tempj.getBurstTime() + tempj.getIOStartTime());
            System.out.format("%5s%11s%11s%10s%15s%10s%12s", tempj.getJobNo(), tempj.getArrival(), tempj.getMemory(),
                    tempj.getOriginalRun(), tempj.getIOStartTime(), tempj.getBurstTime(),
                    tempj.getIOCompleteTime());
            System.out.println();
        }
        System.out.println();
        System.out.println();

    }

    /*
    Prints the status of the simulator at the time it is requested
     */
    void simulationStatus() {
        list eventList = new list();
        int ind = 0;
        while (eventQ.getSize() != 0) {
            Job event = eventQ.dq();
            eventList.insert(event, ind);
            ind++;
        }
        eventList.sortList().print();
        System.out.println("\n************************************************************");
        System.out.println("\nThe status of the simulator at time " + simulationTime + ".\n");
        jobS.printJobQ();
        System.out.println();
        readyQ1.printReadyQ();
        System.out.println();
        readyQ2.printReadyQ2();
        if (!IO.isEmpty()) {
            printIOQ();
        }
        if (IO.isEmpty()) {
            System.out.println();
            System.out.println("The contents of the I/O WAIT QUEUE");
            System.out.println("----------------------------------");
            System.out.println("\nThe I/O Wait Queue is empty.\n\n");
        }
        CPU.printCPU();
        System.out.println();
        finishedList.printFinal(0);
        System.out.println("\n\nThere are " + availMemory + " blocks of main memory available in the system.\n");
    }

}
