
package project356;

/*
 *A complete implementation of singly linked lists
 */
public class list {

    Node head;
    Job job = new Job();
    int count;
    private int size;
    int indX;

    list() {
        head = new Node();
    }

    /* creates a deep copy of the list, will preserve the original */
    list(final list list) {
        if (head == null) {
            head = new Node();
        }
        Node iter1 = head;
        Node iter2 = list.head.next;
        for (; iter2 != null; iter2 = iter2.next) {
            Node temp = new Node(iter2.digit);
            iter1.next = temp;
            iter1 = iter1.next;
        }

    }

    /*
create a new node, and insert it to the position of index
     */
    void insert(final int num, final int index) {
        Node curr = head;
        //traverse list until desired index is 
        for (int i = 0; i < index; i++) {
            curr = curr.next;
        }
        Node newNode = new Node(num, curr.next);
        curr.next = newNode;

    }

    void insert(Job obj, final int index) {
        Node curr = head;
        for (int i = 0; i < index; i++) {
            curr = curr.next;
        }
        Node newNode = new Node(obj, curr.next);
        curr.next = newNode;
        size++;

    }

    //delete a job from the list
    Job delete(final int index) {
        Node curr = head;
        //traverses list but stops at the index-1 because of <
        //curr.next must be used to access name at index-1+1 or index
        for (int i = 0; i < index; i++) {
            curr = curr.next;

        }
        Job theJob = curr.next.job;
        //curr.next (node at index, set to the next node, effectively erasing node at index)
        curr.next = curr.next.next;
        size--;
        return theJob;

    }

    /*
    Updates all the wait times in the job scheduler
     */
    void updateWaitTime() {

        for (Node iter = head.next; iter != null; iter = iter.next) {
            iter.job.setWait(iter.job.getWait() + 1);
        }
    }

    /*
    Prints the contents of the readyQ1
     */
    void printReadyQ() {
        System.out.println("The contents of the FIRST LEVEL READY QUEUE");
        System.out.println("-------------------------------------------\n");
        if (this.size == 0) {
            System.out.println("The First Level Ready Queue is empty.");
        }
        if (this.size > 0) {
            System.out.println("Job #  Arr. Time  Mem. Req.  Run Time");
            System.out.println("-----  ---------  ---------  --------\n");
            for (Node iter = head.next; iter != null; iter = iter.next) {
                System.out.format("%5s%11s%11s%10s", iter.job.getJobNo(), iter.job.getArrival(),
                        iter.job.getMemory(), iter.job.getOriginalRun());
                System.out.println();
            }
        }
        System.out.println();
    }

    /*
    Prints the contents of the readyQ2
     */
    void printReadyQ2() {
        System.out.println("The contents of the SECOND LEVEL READY QUEUE");
        System.out.println("--------------------------------------------");
        if (this.size == 0) {
            System.out.println("\nThe Second Level Ready Queue is empty.");
        }
        if (this.size > 0) {
            System.out.println("\nJob #  Arr. Time  Mem. Req.  Run Time");
            System.out.println("-----  ---------  ---------  --------\n");
            for (Node iter = head.next; iter != null; iter = iter.next) {
                System.out.format("%5s%11s%11s%10s", iter.job.getJobNo(), iter.job.getArrival(),
                        iter.job.getMemory(), iter.job.getOriginalRun());
                System.out.println();
            }
        }
        System.out.println();
    }

    /*
    Prints the contents of the CPU
     */
    void printCPU() {
        System.out.println("The CPU  Start Time  CPU burst time left");
        System.out.println("-------  ----------  -------------------\n");
        if (this.getSize() > 0) {
            for (Node iter = head.next; iter != null; iter = iter.next) {
                if (iter.job.getRun() == 370) {
                    System.out.format("%7s%12s%21s", iter.job.getJobNo(), iter.job.getStartTime(), iter.job.getRun() - 1);
                    System.out.println();
                } else {
                    System.out.format("%7s%12s%21s", iter.job.getJobNo(), iter.job.getStartTime(), iter.job.getRun());
                    System.out.println();
                }
            }
        }
        if (this.size == 0) {
            System.out.println("The CPU is idle.");
        }
    }

    /*
    Checks to see if the list of processes contains IO, returns true/false
     */
    public boolean hasIO() {
        for (Node iter = head.next; iter != null; iter = iter.next) {
            if (iter.job.getEvent().equals("I")) {
                return true;
            }
        }
        return false;
    }

    /*
    Computes the average turnaround time, returns average turnaround time
     */
    double computeTA() {
        double TA = 0;
        int difference = 0;
        for (Node iter = head.next; iter != null; iter = iter.next) {
            difference = iter.job.getCompTime() - iter.job.getArrival();
            TA += difference;
        }
        return Math.round(TA / this.getSize() * 1000d) / 1000d;
    }

    /*
    Computes the average wait time, returns average wait time
     */
    double computeWait() {
        double wait = 0;
        for (Node iter = head.next; iter != null; iter = iter.next) {
            wait += iter.job.getWait();
        }
        return Math.round(wait / this.getSize() * 1000d) / 1000d;
    }

    /*
    Prints the contents of the finished or final finished list, with 0 or 1 as parameter, respectively
     */
    void printFinal(int num) {
        if (num == 0) {
            System.out.println();
            System.out.println("The contents of the FINISHED LIST");
            System.out.println("---------------------------------\n");
            System.out.println("Job # " + " Arr. Time " + " Mem. Req. " + " Run Time " + " Start Time " + " Com. Time");
            System.out.println("-----  ---------  ---------  --------  ----------  ---------\n");
            for (Node iter = head.next; iter != null; iter = iter.next) {
                System.out.format("%5s%11s%11s%10s%12s%11s", iter.job.getJobNo(), iter.job.getArrival(),
                        iter.job.getMemory(), iter.job.getOriginalRun(), iter.job.getStartTime(), iter.job.getCompTime());
                System.out.println();

            }
        }
        if (num == 1) {
            System.out.println();
            System.out.println("The contents of the FINAL FINISHED LIST");
            System.out.println("---------------------------------------\n");
            System.out.println("Job # " + " Arr. Time " + " Mem. Req. " + " Run Time " + " Start Time " + " Com. Time");
            System.out.println("-----  ---------  ---------  --------  ----------  ---------\n");
            for (Node iter = head.next; iter != null; iter = iter.next) {
                System.out.format("%5s%11s%11s%10s%12s%11s", iter.job.getJobNo(), iter.job.getArrival(),
                        iter.job.getMemory(), iter.job.getOriginalRun(), iter.job.getStartTime(), iter.job.getCompTime());
                System.out.println();

            }

        }
    }

    /*
    Prints job contents in the Arrival/Time format
     */
    void print() {
        for (Node iter = head.next; iter != null; iter = iter.next) {
            System.out.println(iter.job.toString());
        }

    }

    /*
        Prints the status of the Job Q
     */
    public void printJobQStatus() {
        System.out.println("The contents of the JOB SCHEDULING QUEUE");
        System.out.println("----------------------------------------");
        if (this.getSize() != 0) {
            System.out.println("\nJob #  Arr. Time  Mem. Req.  Run Time");
            System.out.println("-----  ---------  ---------  --------");
            System.out.println();
            for (Node iter = head.next; iter != null; iter = iter.next) {
                System.out.format("%5s%11s%11s%10s", iter.job.getJobNo(), iter.job.getArrival(),
                        iter.job.getMemory(), iter.job.getOriginalRun());
                System.out.println();
            }
        }
        if (this.getSize() == 0) {
            System.out.println("\nThe Job Scheduling Queue is empty.");
        }
        System.out.println();
    }

    /*
    A peek method to see contents of first node of queue
     */
    Job peak(int index) {
        Node curr = head;
        for (int i = 0; i < index; i++) {
            curr = curr.next;

        }

        Job temp = curr.next.job;
        return temp;

    }

    /*
    Sorts the eventQ based on time of arrival
     */
    public list sortList() {
        list status = new list();
        int index = 0;
        int ind = 0;
        while (this.getSize() != 0) {
            Job temp = this.min();
            for (Node iter = head.next; iter != null && temp != iter.job; iter = iter.next) {
                ind++;
            }
            status.insert(temp, index);
            index++;
            this.delete(ind);
            ind = 0;
        }
        return status;
    }

    /*
    Returns the minimum element of current list, helper method to sortList()
     */
    Job min() {
        Node iter = head.next;
        Job test = head.next.job;
        for (; iter != null; iter = iter.next) {
            if (test.compareTo(iter.job) > 0) {
                test = iter.job;
            }
            if (test.compareTo(iter.job) == 0) {
                if (test.getEvent().equals("T")) {

                } else {
                    test = iter.job;
                }
            }
        }
        return test;
    }

    /*
    Returns the size of the list
     */
    public int getSize() {
        return size;
    }

}
