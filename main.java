/*
/      filename:  Project356.java
/
/   description:  Driver class for job scheduling program

*/
package project356;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.*;
import java.util.*;

public class Project356 {

    /**
     * @param args the command line arguments
     */
    static Scanner cin = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        BufferedReader read;

        Queue pcb = new Queue();
        Queue jobs = new Queue();
        BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in));
        String input;
        while ((input = stdin.readLine()) != null && input.length() != 0) {
            if(input.equals("\n"))break;
            String[] items = input.split("[\\s,]+");
            if (items.length == 5) {
                pcb.addJob(new Job(items[0], Integer.parseInt(items[1]), Integer.parseInt(items[2]),
                        Integer.parseInt(items[3]), Integer.parseInt(items[4])));

            } else if (items.length == 3) {
                pcb.addJob(new Job(items[0], Integer.parseInt(items[1]), Integer.parseInt(items[2])));

            } else if (items.length == 2) {
                pcb.addJob(new Job(items[0], Integer.parseInt(items[1])));
            }
        }
        while (pcb.getSize() != 0) {
            jobs.addJob(pcb.dq());
        }
        Schedule2 sched = new Schedule2(jobs);
        sched.schedule();

    }

}
