/*
/      filename:  Node.java
/
/   description:  Node class for linked list

 */

package project356;


public class Node 
{
  int digit;  
  Node next;    
  Job job;

  /*
  default constructor
  */
  Node()
  {
    digit=0;
    next=null;
    job=null;
  }

  /*
  create new Node from given Node and digit
  */
  Node(final int digit, final Node nextNode)
  {
    this.digit=digit;
    next=nextNode;
    
  }
  /*
  create new node from given Job and node
  */
  Node(Job obj, final Node nextNode)
  {
      this.job=obj;
      next=nextNode;
  }

  /*
  creates a copy of Node
  */
  Node(final Node node)
  {
    
    digit=node.digit;
    next=node;
  }
  /*
  creates Node using a number
  */
  Node(final int digit)
  {
      this.digit=digit;
      next=null;
  }
}
